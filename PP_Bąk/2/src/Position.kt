enum class Position(position:String,level:Int,shortDesc:String) {
    SENIOR("Senior",1,"Master of masters"),
    MID("Regular",2,"Master of mids"),
    JUNIOR("Junior",3,"Junior.");
}