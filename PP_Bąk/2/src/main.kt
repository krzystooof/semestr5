fun main() {
    val numberOfEmployees = 10
    val groupForSalaryCalculating = Position.SENIOR

    val names = listOf("John", "Tom", "Brad", "Erick")
    val surnames = listOf("Muller", "Johnes", "Kowalski", "Kane")

    val company = Company()
    for (number in 1..numberOfEmployees+1){
        val positionIndicator =  ((number.toFloat() / numberOfEmployees) * 100)
        val position = when(positionIndicator.toInt()){
            in 0..30 -> Position.JUNIOR
            in 30..60 -> Position.MID
            else -> Position.SENIOR
        }
        var employee = Employee(names.random(),surnames.random(), position, number*1000F )
        company.addEmployee(employee)
    }
    print(company.toString())

    println("Average salary: ${company.getAverageWage()}")
    println("$groupForSalaryCalculating average salary: ${company.getAverageWage(groupForSalaryCalculating)}")
}

