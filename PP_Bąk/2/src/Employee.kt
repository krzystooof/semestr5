class Employee(name:String, surname:String, val position:Position, val salary:Float) : Person(name, surname){
    override fun toString(): String {
        return "$name $surname, Position: $position, Salary: $salary"
    }
}