class Company():MutableIterable<Employee> {
    private val employeeList = mutableListOf<Employee>()


    fun addEmployee(employee: Employee){
        employeeList.add(employee)
    }
    fun getEmployeesNumber(): Int {
        return employeeList.size
    }

    fun getEmployeesNumber(position: Position): Int {
        val filteredEmployeeList = employeeList.filter { it.position == position }
        return filteredEmployeeList.size
    }

    override fun toString(): String {
        var string = "List of employees:\n"
        employeeList.forEach{
            string+= " * $it\n"
        }
        return string
    }

    override fun iterator(): MutableIterator<Employee> {
        return  employeeList.iterator()
    }

    fun iterator(position: Position): Iterator<Employee> {
        val filteredEmployeeList = employeeList.filter { it.position == position }
        return filteredEmployeeList.iterator()
    }

    fun getAverageWage(): Int {
        var averageSalary = 0
        employeeList.forEach{averageSalary+=it.salary.toInt()}
        averageSalary /= getEmployeesNumber()
        return averageSalary
    }
    fun getAverageWage(position: Position): Int {
        var groupAverageSalary = 0
        iterator(position).forEach { groupAverageSalary+=it.salary.toInt() }
        groupAverageSalary /= getEmployeesNumber(position)
        return groupAverageSalary
    }
}