import heapq
import re
from functools import reduce
from collections import Counter


# 1
def increment(list_of_numbers: list):
    return list(i + 1 for i in list_of_numbers)


tab = [1, 2, 3]
print(f"1: before = {tab}, after = {increment(tab)}")


# 2
def product(list_of_numbers: list) -> list:
    return reduce(lambda x, y: x * y, list_of_numbers)


tab = [1, 2, 3, 4, 5]
print(f"2: before = {tab}, after = {product(tab)}")


# 3
def palindrome(text: str):
    final_text = ""
    text = text.lower()
    if text.isalpha():
        print(text)
    else:
        for char in text:
            if char.isalpha():
                final_text = final_text + char
    return final_text == final_text[::-1]


string = "Tolo ma samolot"
print(f"3: text = {string}, palindrome = {palindrome(string)}")


# 4
def tokenize(text: str):
    tokens_regex = re.compile("\,||\.||\-||\[||\]")
    return list(filter(len, [tokens_regex.sub("", w.lower()) for w in text.split()]))


to_tokenize = "To be, or not to be - that is the question [...]"
print(f"4: before = {to_tokenize}, after = {tokenize(to_tokenize)}")


# 5
def remove_stop_words(words_list: list, stop_words_filename: str = "pl.stopwords.txt"):
    with open(stop_words_filename, encoding="UTF-8") as input_file:
        stop_words = input_file.read()
    return [word for word in words_list if word not in tokenize(stop_words) and len(word) > 2]


words = ["tak", "a", "aby", "było", "krzysztof", "greczka"]
print(f"5: before = {words}, after = {remove_stop_words(words)}")


# 6
def count_most_frequent_words(text: str, n: int):
    text = remove_stop_words(tokenize(text))
    counter = Counter()
    for word in text:
        counter[word] += 1
    return counter.most_common(n)


with open("trurl.txt", encoding="UTF-8") as text_file:
    print(f"6: {count_most_frequent_words(text_file.read(), 20)}")


# 7
def get_anagrams(words_list: list):
    hashed = set()
    result = []
    for word in words_list:
        ordered_word = "".join(sorted(word))
        if ordered_word in hashed:
            result.append(word)
        else:
            hashed.add(ordered_word)
    return result


with open("unixdict.txt", "r") as words_file:
    anagrams = get_anagrams(tokenize(" ".join(words_file.read().splitlines())))
    print(f"7: {heapq.nlargest(10, anagrams, len)}")
