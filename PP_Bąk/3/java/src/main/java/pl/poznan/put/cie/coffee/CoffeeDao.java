package pl.poznan.put.cie.coffee;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import pl.poznan.put.cie.coffee.DbUtilities;
import pl.poznan.put.cie.coffee.Coffee;

import javax.sql.DataSource;

public class CoffeeDao {

	private final NamedParameterJdbcTemplate jdbc;
	private static String url = "jdbc:mysql://localhost:3306/coffee?serverTimezone=UTC";
	private static String username = "root";
	private static String password = "admin";

	public CoffeeDao() {
		DataSource dataSource = DbUtilities.getDataSource(url, username, password);
		this.jdbc = new NamedParameterJdbcTemplate(dataSource);
	}

	/**
	 * Returns a coffee with given name.
	 *
	 * @param name coffee name
	 * @return coffee object or null
	 */
	public Coffee get(String name) {
		String sql = "SELECT sup_id, price, sales, total FROM COFFEES "
				  + "WHERE cof_name = :cof_name";
		MapSqlParameterSource params = new MapSqlParameterSource("cof_name", name);

		return jdbc.query(sql, params, result -> {
			if (result.next()) {
				return new Coffee(
						name,
						result.getInt("sup_id"),
						new BigDecimal(result.getString("price")),
						result.getInt("sales"),
						result.getInt("total")
				);

			} else {
				return new Coffee();
			}
		});
	}

	/**
	 * Returns a list of all coffees.
	 *
	 * @return list of all coffees
	 */
	public List<Coffee> getAll() {
		String sql = "SELECT cof_name, sup_id, price, sales, total FROM COFFEES";
		try {
			return jdbc.query(sql, result -> {
				List<Coffee> coffeeList = new ArrayList<>();
				while(result.next()) {
					coffeeList.add(new Coffee(
							result.getString("cof_name"),
							result.getInt("sup_id"),
							new BigDecimal(result.getString("price")),
							result.getInt("sales"),
							result.getInt("total")
					));
				}
				return coffeeList;
			});
		} catch (EmptyResultDataAccessException ex) {
			return new ArrayList<>();
		}
	}

	public void update(Coffee c) {
		String sql = "UPDATE COFFEES "
				  + "SET price = :price, sales = :sales, total = :total "
				  + "WHERE cof_name = :cof_name AND sup_id = :sup_id";
		insertOrUpdate(sql, c);
	}

	public void delete(String name, int supplierId) {
		String sql = "DELETE FROM COFFEES WHERE cof_name = :cof_name AND sup_id = :sup_id ";
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("sup_id", supplierId);
		parameters.put("cof_name", name);
		jdbc.update(sql, parameters);
	}

	public void create(Coffee c) {
		String sql = "INSERT INTO COFFEES(cof_name, sup_id, price, sales, total) VALUES(:cof_name, :sup_id, :price, :sales, :total )";
		insertOrUpdate(sql, c);
	}

	private void insertOrUpdate(String sql, Coffee c) {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("cof_name", c.getName());
		parameters.put("price", c.getPrice());
		parameters.put("sales", c.getSales());
		parameters.put("total", c.getTotal());
		parameters.put("sup_id", c.getSupplierId());
		jdbc.update(sql, parameters);
	}
}
