package pl.krzystooof.shop.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "COFFEES")
//@Data
@Getter
@Setter
public class Coffee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COF_NAME")
    private String name;

    @Column(name = "SUP_ID")
    private int supplierId;

    @Column(name = "PRICE")
    private double price;

    @Column(name = "SALES")
    private int sales;

    @Column(name = "TOTAL")
    private int total;
}
