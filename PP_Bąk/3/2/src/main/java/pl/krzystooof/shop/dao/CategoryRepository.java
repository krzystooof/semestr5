package pl.krzystooof.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import pl.krzystooof.shop.entity.Coffee;

@RepositoryRestResource(collectionResourceRel = "coffee", path = "coffees")
public interface CategoryRepository extends JpaRepository<Coffee,String> {
}
