package pl.poznan.put.gol.game;

public class ConwaysRules implements Rules {

    @Override
    public boolean inNextGeneration(boolean alive, int numberOfNeighbors) {
        if (!alive) {
            return numberOfNeighbors == 3;
        } else return numberOfNeighbors == 2 || numberOfNeighbors == 3;
    }
}
