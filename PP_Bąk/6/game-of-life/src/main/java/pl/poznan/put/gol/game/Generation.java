package pl.poznan.put.gol.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Generation {

    private Rules rules;
    private Cells aliveCells;

    public Generation(Rules rules, Cell... aliveCells) {
        this(rules, new Cells(aliveCells));
    }

    public Generation(Rules rules, Cells aliveCells) {
        this.rules = rules;
        this.aliveCells = aliveCells;
    }

    public void evolve() {
        Cells nextAliveCells = new Cells();
        aliveCells.getNeighbors().forEach(cell -> {
                    if (isAlive(cell)) nextAliveCells.add(cell);
                }
        );
        aliveCells = nextAliveCells;
    }

    public boolean isAlive(Cell cell) {
        return rules.inNextGeneration(aliveCells.contains(cell), countAliveNeighbors(cell));
    }

    public int countAliveNeighbors(Cell cell) {
        List<Cell> aliveNeighbors = new ArrayList<>(aliveCells.cells);
        aliveNeighbors.retainAll(cell.neighbors().cells);
        return aliveNeighbors.size();
    }

    public boolean extinct() {
        return aliveCells.isEmpty();
    }

    public Cells getAliveCells() {
        return aliveCells;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Generation)) {
            return false;
        }
        Generation other = (Generation) obj;
        return aliveCells.equals(other.aliveCells);
    }
}
