package pl.poznan.put.gol.game;

import org.junit.Assert;
import org.junit.Test;

public class ConwaysCellTest {

    @Test
    public void neighborNumberShouldEqualZero() {
        ConwaysCell cell = new ConwaysCell(0, 0);
        Assert.assertEquals(cell.neighbors().size(), 8);
    }

    @Test
    public void neighborNumberShouldEqual3() {
        Cells actual = new ConwaysCell(0, 0).neighbors();
        Cells expected = new Cells(
                new ConwaysCell(0, 1),
                new ConwaysCell(1, 1),
                new ConwaysCell(1, 0)
        );

        Assert.assertEquals(actual, expected);
    }

    @Test
    public void neighborNumberShouldEqual8() {
        Cells actual = new ConwaysCell(2, 2).neighbors();
        Cells expected = new Cells(
                new ConwaysCell(1, 1),
                new ConwaysCell(1, 2),
                new ConwaysCell(1, 3),
                new ConwaysCell(2, 1),
                new ConwaysCell(2, 3),
                new ConwaysCell(3, 1),
                new ConwaysCell(3, 2),
                new ConwaysCell(3, 3)
        );

        Assert.assertEquals(actual, expected);
    }

}