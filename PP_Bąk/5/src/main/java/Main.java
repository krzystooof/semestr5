import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.TopDocs;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            MyAnalyzer analyzer = new MyAnalyzer(1000);

            // Znajdź przedmioty" zawierający słowo 𝛼, a nie zawierające słowa 𝛽 w tytule. (Fields, Boolean Operators)
            runQueryAndPrintResult(analyzer, "name", "Gruszka AND NOT Silikonowa");

            // Znajdź przedmioty zawierający słowo 𝛼, a nie zawierające słowa 𝛽 w tytule. (Fields, Boolean Operators)
            runQueryAndPrintResult(analyzer, "name", "name:Gruszka AND description:(Silikonowa OR mała)");

            // Znajdź przedmioty zawierające wyrażenie zaczynające się od 𝛼 w nazwie kategorii. (Wildcard Searches)
            runQueryAndPrintResult(analyzer, "category", "Ba*");

            // Znajdź przedmioty zawierające słowa podobne (tj. różniące się maksymalnie 2 literami) do 𝛼 w tytule (Fuzzy Searches).
            runQueryAndPrintResult(analyzer, "name", "Fil~2");

            //Znajdź przedmioty, posortowane rosnąco wg ceny, których cena jest mniejsza niż 𝛼 i większa niż 𝛽. (Range Searches)
            printTotalHits("Price between 10 and 100", analyzer.find("price", 10, 100));

        } catch (XMLStreamException | IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private static void runQueryAndPrintResult(MyAnalyzer analyzer, String field, String query) throws IOException, ParseException {
        TopDocs results = analyzer.find(field, query);
        printTotalHits(query, results);
    }


    private static void printTotalHits(String query, TopDocs results) {
        System.out.println(query + ": " + results.totalHits);
    }
}
