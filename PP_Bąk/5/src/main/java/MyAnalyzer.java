import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.FloatPoint;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;

import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.nio.file.Files;


public class MyAnalyzer {
    private StandardAnalyzer analyzer;
    Directory index;
    IndexReader reader;
    IndexSearcher searcher;
    int hitsPerPage;

    public MyAnalyzer(int hitsPerPage) throws IOException, XMLStreamException {
        this.hitsPerPage = hitsPerPage;
        analyzer = new StandardAnalyzer();
        index = new MMapDirectory(Files.createTempDirectory("PP").toAbsolutePath());
        indexItems(analyzer, index);
        reader = DirectoryReader.open(index);
        searcher = new IndexSearcher(reader);
    }

    public TopDocs find(String field, String queryString) throws ParseException, IOException {
        Query query = new QueryParser(field, analyzer).parse(queryString);
        return searcher.search(query, hitsPerPage);
    }

    public TopDocs find(String field, float minValue, float maxValue ) throws IOException {
        Query query = FloatPoint.newRangeQuery(field, minValue, maxValue);
        return searcher.search(query,hitsPerPage);
    }

    public static void indexItems(Analyzer analyzer, Directory directory) throws IOException, XMLStreamException {
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter writer = new IndexWriter(directory, config);

        ItemProvider provider = new ItemProvider("items.xml");
        while (provider.hasNext()) {
            Item item = provider.next();
            writer.addDocument(item.toLuceneDocument());
        }
        writer.close();
    }
}
