import java.lang.Math.random
import kotlin.system.measureTimeMillis

fun insertionSort(items:MutableList<Int>):List<Int>{
    if (items.isEmpty() || items.size<2){
        return items
    }
    for (count in 1 until items.count()){
        val item = items[count]
        var i = count
        while (i>0 && item < items[i - 1]){
            items[i] = items[i - 1]
            i -= 1
        }
        items[i] = item
    }
    return items
}

fun mergeSort(list: List<Int>): List<Int> {
    if (list.size <= 1) {
        return list
    }

    val middle = list.size / 2
    var left = list.subList(0,middle);
    var right = list.subList(middle,list.size);

    return merge(mergeSort(left), mergeSort(right))
}
fun merge(left: List<Int>, right: List<Int>): List<Int>  {
    var indexLeft = 0
    var indexRight = 0
    var newList : MutableList<Int> = mutableListOf()

    while (indexLeft < left.count() && indexRight < right.count()) {
        if (left[indexLeft] <= right[indexRight]) {
            newList.add(left[indexLeft])
            indexLeft++
        } else {
            newList.add(right[indexRight])
            indexRight++
        }
    }

    while (indexLeft < left.size) {
        newList.add(left[indexLeft])
        indexLeft++
    }

    while (indexRight < right.size) {
        newList.add(right[indexRight])
        indexRight++
    }
    return newList;
}

fun main() {
    val listSize = 10000000n
    val random:List<Int> = Array(listSize) { random().toInt()}.toList()
    val ascending = random.sorted()
    val descending = ascending.reversed()

    val insertion = mapOf(
            "descending->ascending" to measureTimeMillis {insertionSort(descending as MutableList<Int>)},
            "ascending->ascending" to measureTimeMillis {insertionSort(ascending as MutableList<Int>)},
            "random->ascending" to measureTimeMillis {insertionSort(random as MutableList<Int>)}
    )

    println("Insertion sort time:")
    insertion.forEach{ (key,value)->
        println(" * $key = $value ms")
    }

    val merge = mapOf(
            "descending->ascending" to measureTimeMillis {mergeSort(descending)},
            "ascending->ascending" to measureTimeMillis {mergeSort(ascending)},
            "random->ascending" to measureTimeMillis {mergeSort(random)}
    )

    println("Merge sort time:")
    merge.forEach{ (key,value)->
        println(" * $key = $value ms")
    }

}