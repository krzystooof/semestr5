import java.math.BigInteger
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import kotlin.system.measureTimeMillis

fun factorial(number: Int): BigInteger {
    var result = 1.toBigInteger()
    for (i in 2..number) {
        result *= i.toBigInteger()
    }
    return result
}

fun measureResults(number: Int): Map<String, String> {
    var factorial: BigInteger = 0.toBigInteger()
    val time = measureTimeMillis { factorial = factorial(number) }
    val formatter = DecimalFormat("0.#####E0", DecimalFormatSymbols.getInstance(Locale.ROOT))
    val factorialInScientificNotation = formatter.format(factorial)
    return mapOf(
            "factorial" to factorialInScientificNotation.toString(),
            "time" to time.toString()
    )
}

fun main() {
    //    if factorialOf = 0, looping to find given values
    var factorialOf = 0

    //loop settings
    val loopStart = 100000
    val maxTimeInMilliseconds = 5000
    val step = 0.1 //fraction of loopStart increasing factorial value

    when (factorialOf) {
        0 -> {
            factorialOf = loopStart
            var results = measureResults(factorialOf)
            while (results["time"].toString().toInt() < maxTimeInMilliseconds) {
                println("Calculating time of <$factorialOf! = ${results["factorial"]}> is ${results["time"]} ms")
                factorialOf += (step * loopStart).toInt()
                results = measureResults(factorialOf)
            }
        }
        else -> {
            val results = measureResults(factorialOf)
            println("Calculating time of <$factorialOf! = ${results["factorial"]}> is ${results["time"]} ms")
        }
    }
}