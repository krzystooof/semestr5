import java.io.File

fun countWords(text: String): MutableMap<String, Int> {
    val listOfWords: List<String> = text.toLowerCase()
            .replace(Regex("[^A-Za-z0-9 ]"), "")
            .split(Regex("\\s+"))
            .filter { it.length >= 3 }

    val listOfUniqueWords = listOfWords.distinct()

    val wordsCount = mutableMapOf<String, Int>()
    listOfUniqueWords.forEach{ uniqueWord ->
        wordsCount[uniqueWord]=listOfWords.count{
            it == uniqueWord
        }
    }
    return wordsCount
}

fun main() {
    val fileName = "resources/macbeth.txt"

    val input = File(fileName).readLines().toString()
    val mapOfUniqueWords = countWords(input).toList()
            .sortedBy { (key, value) -> value }
            .reversed()
            .toMap()

    var sum = mapOfUniqueWords.count()

    println("Sum of unique words in <$fileName>: $sum")

    println("Map of unique words in <$fileName>: $mapOfUniqueWords")
    println("Map of 8 most frequently occurring unique words in <$fileName>: ${mapOfUniqueWords.toList().subList(0,8).toMap()}")

}