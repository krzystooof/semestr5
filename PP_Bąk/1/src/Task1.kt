import java.util.*
import kotlin.collections.ArrayList
import kotlin.system.measureTimeMillis

fun main() {
    val size = 10000000
    val times = mapOf(
            "array" to measureTimeMillis {
                Array(size) {0}
            },
            "arrayList" to measureTimeMillis {
                var array = ArrayList<Int>()
                for (number in 0..size){
                    array.add(0)
                }
            },
            "arrayListWithGivenSize" to measureTimeMillis {
                ArrayList<Int>(size)
            },
            "linkedList" to measureTimeMillis {
                var array = LinkedList<Int>()
                for (number in 0..size){
                    array.add(0)
                }
            }
    )
    println("Creating time:")
    times.forEach{ (key,value)->
        println(" * $key = $value ms")
    }
}