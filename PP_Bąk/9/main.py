import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_boston
from sklearn.metrics import mean_squared_error, mean_absolute_error

boston_dataset = load_boston()
print(f"* {boston_dataset.keys()}")

data_frame = pd.DataFrame(boston_dataset.data)
data_frame.columns = boston_dataset.feature_names
data_frame['MEDV'] = boston_dataset.target
print("* 10 first records:")
print(data_frame.head(10))
print("* 10 last records:")
print(data_frame.tail(10))

print(f"*")
data_frame.info()
# 506 samples, all columns contains 'float64' type of data, all colums are non-null

print(f"*\n {data_frame.describe()}")
# CRIM: mean = 3.6, std = 8.6; MEDV: min = 5, max = 50; LSTAT: 50% = 11.36

seaborn.displot(data_frame['MEDV']).savefig("MEDV.png")
plt.clf()

corr = data_frame.corr()
seaborn.heatmap(corr, linewidths=.5, cmap="YlGnBu")
plt.savefig("CORR.png", dpi=200)
plt.clf()

for column in data_frame.columns:
    seaborn.regplot(x=data_frame['MEDV'], y=data_frame[f"{column}"])
    plt.savefig(f"MEDVx{column}.png", dpi=200)
    plt.clf()
# + correlated: ZN, CHAS, RM, DIS, B, - correlated: CRIM, INDUS, NOX, AGE, RAD, TAX, TRATIO, min correlated: LSTAT

x = data_frame[['ZN', 'CHAS', 'RM', 'DIS', 'B']]
y = data_frame['MEDV']
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)
regression = LinearRegression()
regression.fit(x_train, y_train)
y_pred = regression.predict(x_test)
plt.scatter(x=y_test, y=y_pred)
plt.savefig(f"REALxPRED.png", dpi=200)
plt.clf()

print(f"Pred RMSE: {mean_squared_error(y_test, y_pred)}")
print(f"Pred MAE: {mean_absolute_error(y_test, y_pred)}")
