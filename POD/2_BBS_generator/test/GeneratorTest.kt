import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GeneratorTest {

    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @Nested
    class FIPS140_2 {
        private val chain = Generator().generate()

        @Test
        fun `single bit - 1 `() {
            // number of '1' in 20 000 bit list should be between 9725 and 10275
            val numberOfOnes: Int = chain.count { it == '1' }
            assertEquals(true, numberOfOnes in 9726..10274)
        }

        @Test
        fun `single bit - 0 `() {
            // number of '0' in 20 000 bit list should be between 9725 and 10275
            val numberOfOnes: Int = chain.count { it == '0' }
            assertEquals(true, numberOfOnes in 9726..10274)
        }

        @Test
        fun `series - 1`() {
            val series = getSeries('1')
            assertEquals(true, series[0] in 2315..2685)
            assertEquals(true, series[1] in 1114..1386)
            assertEquals(true, series[2] in 527..723)
            assertEquals(true, series[3] in 240..384)
            assertEquals(true, series[4] in 103..209)
            assertEquals(true, series[5] in 103..209)
        }

        @Test
        fun `series - 0`() {
            val series = getSeries('0')
            assertEquals(true, series[0] in 2315..2685)
            assertEquals(true, series[1] in 1114..1386)
            assertEquals(true, series[2] in 527..723)
            assertEquals(true, series[3] in 240..384)
            assertEquals(true, series[4] in 103..209)
            assertEquals(true, series[5] in 103..209)
        }

        @Test
        fun `long series`() {
            var passed = true
            var elementNumber = 0
            while (elementNumber < chain.size - 1) {
                var seriesLength = getSeriesLength(elementNumber)
                if (seriesLength > 25) passed = false
                elementNumber += seriesLength
            }
            assertEquals(true, passed)
        }

        @Test
        fun `poker`() {
            var segmentNumber = 1
            val numberOfElementsInSegment = 4
            val segmentsList = mutableListOf<String>()
            while (numberOfElementsInSegment * segmentNumber <= chain.size) {
                val lastElementIndex = numberOfElementsInSegment * segmentNumber - 1
                var byte = ""
                for (elementIndex in lastElementIndex - 3..lastElementIndex) {
                    byte += chain[elementIndex]
                }
                segmentsList.add(byte)
                segmentNumber++
            }
            val numberOfEqualSegments = mapOf(
                "0000" to segmentsList.count { it == "0000" },
                "0001" to segmentsList.count { it == "0001" },
                "0010" to segmentsList.count { it == "0010" },
                "0011" to segmentsList.count { it == "0011" },
                "0100" to segmentsList.count { it == "0100" },
                "0101" to segmentsList.count { it == "0101" },
                "0110" to segmentsList.count { it == "0110" },
                "0111" to segmentsList.count { it == "0111" },
                "1000" to segmentsList.count { it == "1000" },
                "1001" to segmentsList.count { it == "1001" },
                "1010" to segmentsList.count { it == "1010" },
                "1011" to segmentsList.count { it == "1011" },
                "1100" to segmentsList.count { it == "1100" },
                "1101" to segmentsList.count { it == "1101" },
                "1110" to segmentsList.count { it == "1110" },
                "1111" to segmentsList.count { it == "1111" }
            )
            var sumOfNumberOfSegmentsSquared = 0
            numberOfEqualSegments.forEach { (key, value) ->
                sumOfNumberOfSegmentsSquared += value * value
            }
            val parameterX = ((16/5000F)*sumOfNumberOfSegmentsSquared)-5000
            assertEquals(true, parameterX in 2.16..46.17)
        }

        private fun getSeries(wantedElement: Char): MutableList<Int> {
            val series = MutableList(6) { 0 }
            var elementNumber = 0
            while (elementNumber < chain.size - 1) {
                if (chain[elementNumber] == wantedElement) {
                    var seriesLength = getSeriesLength(elementNumber)
                    elementNumber += seriesLength
                    if (seriesLength > 6) seriesLength = 6 //6 and more is the same interval
                    series[seriesLength - 1]++
                } else elementNumber++
            }
            return series
        }

        private fun getSeriesLength(firstElementNumber: Int): Int {
            var elementNumber = firstElementNumber
            var seriesLength = 1
            while (elementNumber < chain.size - 1 && checkIfNextElementIsEqual(elementNumber)) {
                seriesLength++
                elementNumber++
            }
            return seriesLength
        }

        private fun checkIfNextElementIsEqual(elementNumber: Int): Boolean {
            return chain[elementNumber + 1] == chain[elementNumber]
        }
    }

    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @Nested
    class StaticMethods {
        @Test
        fun `primeNumbers - should always return true`() {
            val knownPrimeNumbers = listOf(2, 3, 5, 7, 47, 79, 97, 127, 211, 257, 271)
            knownPrimeNumbers.forEach {
                assertEquals(true, Generator.isPrimeNumber(it.toLong()))
            }
        }

        @Test
        fun `primeNumbers - should always return false`() {
            val knownNotPrimeNumbers = listOf(1, 4, 6, 8, 15, 48, 77, 92, 123, 210, 253, 275)
            knownNotPrimeNumbers.forEach {
                assertEquals(false, Generator.isPrimeNumber(it.toLong()))
            }
        }


        @Test
        fun `modulo - should always return true`() {
            val knownModuloNumbers = listOf(3, 11, 15, 43, 255, 403)
            knownModuloNumbers.forEach {
                assertEquals(true, Generator.isModulo4EqualTo3(it.toLong()))
            }
        }

        @Test
        fun `modulo - should always return false`() {
            val knownNotModuloNumbers = listOf(1, 4, 6, 14, 253, 274)
            knownNotModuloNumbers.forEach {
                assertEquals(false, Generator.isModulo4EqualTo3(it.toLong()))
            }
        }

        @Test
        fun `GKD - shouldReturn 1`() {
            assertEquals(1.toBigInteger(), Generator.greatestCommonDivisor(25L, 27L))
            assertEquals(1.toBigInteger(), Generator.greatestCommonDivisor(15L, 26L))
            assertEquals(1.toBigInteger(), Generator.greatestCommonDivisor(15L, 28L))
        }
    }


}