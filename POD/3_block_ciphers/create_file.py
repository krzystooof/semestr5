def create_file_bytes(size_bytes: int, filename: str):
    with open(filename, "wb") as out:
        out.seek(size_bytes - 1)
        out.write(b"\0")

if __name__ == '__main__':
    create_file_bytes(250000000, "250_mb_file")
