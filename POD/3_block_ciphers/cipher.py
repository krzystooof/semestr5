from enum import Enum
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES


class CipherType(Enum):
    ECB = 1
    CBC = 2
    OFB = 3
    CFB = 4
    CTR = 5


class Cipher:
    def __init__(self, cipher_type: CipherType = CipherType.ECB):
        if cipher_type == CipherType.ECB:
            self.cipher_mode = AES.MODE_ECB
        elif cipher_type == CipherType.CBC:
            self.cipher_mode = AES.MODE_CBC
        elif cipher_type == CipherType.OFB:
            self.cipher_mode = AES.MODE_OFB
        elif cipher_type == CipherType.CFB:
            self.cipher_mode = AES.MODE_CFB
        elif cipher_type == CipherType.CTR:
            self.cipher_mode = AES.MODE_CTR

    def encrypt(self, input_data: str, key: str):
        cipher = AES.new(key, self.cipher_mode)
        return cipher.encrypt(input_data)

    def decrypt(self, input_data: str, key: str):
        cipher = AES.new(key, self.cipher_mode)
        return cipher.decrypt(input_data)

    @staticmethod
    def generate_random_key(length: int = 16):
        return get_random_bytes(length)
