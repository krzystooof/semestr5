#!/usr/bin/env python
import sys

from cipher import CipherType, Cipher
from time_count import TimeCount


class WorkMode(enumerate):
    ENCRYPT = 1
    DECRYPT = 2


def count_operation_time_milis(mode: WorkMode, cipher: Cipher, input, key: str):
    time = TimeCount()
    if mode == WorkMode.ENCRYPT:
        cipher.encrypt(input, key)
    else:
        cipher.decrypt(input, key)
    milis = time.get_elapsed_time_milis()
    return milis


if __name__ == '__main__':
    try:
        file_location = sys.argv[1]
    except IndexError:
        file_location = input("Input file location not provided in arguments.\nInput file location: ")
    try:
        key_length = int(sys.argv[2])
    except IndexError:
        key_length = int(input("Key length not provided in arguments.\nKey length: "))

    key = Cipher.generate_random_key(key_length)
    with open(file_location, 'r') as file:
        input = file.read().encode("utf-8")
        for type in CipherType:
            print("-> " + str(type) + ":")
            cipher = Cipher(type)

            print("  encrypting...")
            elapsed_time = count_operation_time_milis(WorkMode.ENCRYPT, cipher, input, key)
            print("  Done in " + str(elapsed_time) + "ms")

            print("  decrypting...")
            elapsed_time = count_operation_time_milis(WorkMode.DECRYPT, cipher, input, key)
            print("  Done in " + str(elapsed_time) + "ms")
