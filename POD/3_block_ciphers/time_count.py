import time


class TimeCount:
    def __init__(self):
        self.start_time = int(round(time.time() * 1000))

    def get_elapsed_time_milis(self):
        return int(round(time.time() * 1000)) - self.start_time
