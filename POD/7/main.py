import hashlib
import os
import random
import time
from pprint import pprint
import pandas as pd

FILE_SIZES = [100, 250, 500, 1000, 2500, 5000]
CHAR_LIMIT = (64, 255)

hash_functions = {
    "MD5": hashlib.md5,
    "SHA-1": hashlib.sha1,
    "SHA2(256)": hashlib.sha256,
    "SHA2(384)": hashlib.sha384,
    "SHA2(512)": hashlib.sha512,
    "SHA-3(224)": hashlib.sha3_224,
    "SHA-3(256)": hashlib.sha3_256,
    "SHA-3(384)": hashlib.sha3_384,
    "SHA-3(512)": hashlib.sha3_512,
}


def get_hash_and_time_nanos(method: str, data: bytes) -> tuple:
    start_time = time.time_ns()
    result = hash_functions[method](data).hexdigest()
    return result, time.time_ns() - start_time


def generate_random_string(length: int) -> str:
    string = ""
    for _ in range(length):
        random_integer = random.randint(CHAR_LIMIT[0], CHAR_LIMIT[1])
        string += (chr(random_integer))
    return string


def generate_file(filename: str, size_chars: int):
    with open(filename, "w") as out:
        out.write(generate_random_string(size_chars))


def remove_file(filename: str):
    os.remove(filename)


if __name__ == '__main__':
    results = {}
    for size in FILE_SIZES:
        generate_file(str(size), size)
        results[f"{size} chars"] = {}
        for name, func in hash_functions.items():
            with open(str(size), "rb") as file:
                results[f"{size} chars"][name] = get_hash_and_time_nanos(name, file.read())[1]
        remove_file(str(size))

pprint(results)
print(pd.DataFrame(results))