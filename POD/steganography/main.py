from PIL import Image

source_file = "tvpis.jpg"
message = "Niemcy zazdroszcza p0lsce praworzadnosci"
stop_char = '>'

image = Image.open(source_file)
width, height = image.size
pixels = image.load()
print(f"Original message: {message}")
message += stop_char

binary_message = ''.join(format(ord(i), 'b').zfill(8) for i in message)
i = 0
for row in range(width):
    for column in range(height):
        if i < len(binary_message):
            r, g, b = pixels[row, column]
            r_binary = format(r, 'b').zfill(8)
            new_r_binary = r_binary[:-2] + binary_message[i:i + 2]
            new_r_int = int(new_r_binary, 2)
            pixels[row, column] = (new_r_int, g, b)
            i += 2

image.show()

width, height = image.size
pixels = image.load()
char = ''
string = ""
char_binary = ''
for row in range(width):
    for column in range(height):
        r, _, _ = pixels[row, column]
        r_binary = format(r, 'b').zfill(8)
        if len(char_binary) == 8:
            char = chr(int(char_binary, 2))
            if char == stop_char:
                print(f"Decoded message : {string}")
                exit(0)
            string += char
            char_binary = ''
        char_binary += r_binary[-2:]



