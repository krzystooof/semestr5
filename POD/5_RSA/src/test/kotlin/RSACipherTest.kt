import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class RSACipherTest{

    @Test
    fun cipherAndDecipher(){
        val expected = "To jest tekst do zaszyfrowania. Krzysztof Greczka "

        val RSA = RSACipher(1009,1013)
        val ciphered = RSA.cipherText(expected)
        val deciphered = RSA.decipherText(ciphered)

        assertEquals(expected,deciphered)
    }
}