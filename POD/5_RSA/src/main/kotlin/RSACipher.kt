import java.lang.Exception
import java.math.*;

class RSACipher(parameterP: Int = 31, parameterQ: Int = 19) {
    init {
        if (!Prime.isPrime(parameterP)) throw Exception("Parameter P <$parameterP> is not a prime, next prime is <${Prime.nextInt(parameterP)}>")
        if (!Prime.isPrime(parameterQ)) throw Exception("Parameter Q <$parameterQ> is not a prime, next prime is <${Prime.nextInt(parameterQ)}>")
        if (!Prime.isPrime(parameterQ)) throw Exception("Parameter Q <$parameterQ> can not be equal to parameter P <$parameterP>")
    }
    private val parameterN = (parameterP * parameterQ)
    private val parameterPHI = ((parameterP - 1) * (parameterQ - 1))
    private val parameterE = Prime.nextCoprimeWith(parameterPHI)
    private val parameterD = generateParameterD()


    private fun generateParameterD(): Int {
        var currentNumber = 0
        while((parameterE * currentNumber - 1) % parameterPHI.toLong() != 0.toLong()){
            currentNumber++
        }
        return currentNumber
    }

    fun cipherText(text: String): List<BigInteger> {
        return text.toByteArray().map { cipherByte(it) }
    }

    fun decipherText(text: List<BigInteger> ): String {
        val bytes = text.map { decipherByte(it) }
        var string = ""
        bytes.forEach{
            string += it.toChar()
        }
        return string
    }

    fun cipherByte(byte: Byte): BigInteger {
        val pow = Math.pow(byte.toDouble(), parameterE.toDouble())
        val resultDouble = pow % parameterN.toDouble()
        return resultDouble.toInt().toBigInteger()
    }

    fun decipherByte(byte:BigInteger): Byte {
        val resultBigInteger = byte.modPow(parameterD.toBigInteger(),parameterN.toBigInteger())
        return resultBigInteger.toByte()
//        val pow = byte.pow(parameterD)
//        val resultDouble = pow % parameterN.toBigInteger()
//        return resultDouble.toInt().toByte()
    }


}