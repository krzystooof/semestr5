import java.math.BigInteger

class Prime {
    companion object {
        fun isPrime(number: Int): Boolean {
            for (i in 2 until number) {
                if (number % i == 0) {
                    return false
                }
            }
            return true
        }

        fun nextInt(startFrom: Int = 0): Int {
            var currentNumber = startFrom
            while(!isPrime(currentNumber)){
               currentNumber++
            }
            return currentNumber
        }

        fun nextCoprimeWith(number:Int,startFrom: Int = 2): Int {
            var currentPrime = nextInt(startFrom)
            while (number.toBigInteger().gcd(currentPrime.toBigInteger()) != 1.toBigInteger())
                currentPrime = nextInt(currentPrime+1)
            return currentPrime
        }
    }
}