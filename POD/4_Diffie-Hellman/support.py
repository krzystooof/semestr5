from math import gcd


def get_primitive_root(modulo):
    coprime_set = {num for num in range(1, modulo) if gcd(num, modulo) == 1}
    for number in range(3, modulo):
        if coprime_set == {pow(number, powers, modulo) for powers in range(1, modulo)}:
            return number
