from random import randrange

import sympy.ntheory as nt

from support import get_primitive_root


class Client:
    def __init__(self, prime_bigger_than: int = 10 ** 5):
        self.parameter_n = nt.nextprime(prime_bigger_than)
        self.parameter_g = get_primitive_root(self.parameter_n)
        self.private_key = randrange(prime_bigger_than)
        self.public_key = (self.parameter_g ** self.private_key) % self.parameter_n

    def generate_session_key(self, other_client) -> int:
        return (other_client.public_key ** self.private_key) % self.parameter_n
