from client import Client


if __name__ == '__main__':
    clientA = Client()
    clientB = Client()
    print(f"A: private - {clientA.private_key}, public - {clientA.public_key}, session - "
          f"{clientA.generate_session_key(clientB)}")
    print(f"B: private - {clientB.private_key}, public - {clientB.public_key}, session - "
          f"{clientB.generate_session_key(clientA)}")

