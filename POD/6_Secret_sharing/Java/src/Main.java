import java.math.BigInteger;
import java.util.Map;
import java.util.Random;


public class Main {
    public static void main(String[] args) {
        int n = 15, t = 10;
        int bitLength = 50;

        BigInteger secret = new BigInteger(bitLength, new Random());
        BigInteger prime = new BigInteger(secret.bitLength() + 1, 50, new Random());
        ShamirSecretSharing shamirSecretSharing = new ShamirSecretSharing(n, t, prime);

        System.out.println("Shares: " + n);
        System.out.println("Min shares: " + t);
        System.out.println("Secret: " + secret);
        System.out.println("Prime: " + prime);
        Map<Integer, BigInteger> shares = shamirSecretSharing.splitSecret(secret);

        System.out.println("Shares: ");
        shares.entrySet().stream()
                .forEach(share -> System.out.println(" * " + share.getKey() + ":" + share.getValue()));

        BigInteger mergedSecret = shamirSecretSharing.mergeShares(shares);
        System.out.println("Secret merged using all (" + n + ") shares: " + mergedSecret);

        t = 10;
        shamirSecretSharing = new ShamirSecretSharing(n, t, prime);
        BigInteger mergedShares = shamirSecretSharing.mergeShares(shares);
        System.out.println("Secret merged using " + t + " shares: " + mergedShares);
    }
}
