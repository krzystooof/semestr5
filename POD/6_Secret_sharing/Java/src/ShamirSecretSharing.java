import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ShamirSecretSharing {
    private final int parameterN;
    private final int parameterT;
    private final BigInteger parameterP;

    protected ShamirSecretSharing(int parameterN, int parameterT, BigInteger prime) {
        this.parameterN = parameterN;
        this.parameterT = parameterT;
        this.parameterP = prime;
    }

    protected Map<Integer, BigInteger> splitSecret(final BigInteger secret) {
        BigInteger[] tArray = new BigInteger[parameterT - 1];
        Map<Integer, BigInteger> shares = new HashMap<>();

        for (int i = 0; i < parameterT - 1; i++)
            tArray[i] = new BigInteger(parameterP.bitLength(), new Random());

        for (int i = 1; i <= parameterN; i++) {
            BigInteger shareValue = secret;
            for (int j = 1; j < parameterT; j++) {
                BigInteger x = BigInteger.valueOf(i).pow(j);
                BigInteger a = tArray[j - 1].multiply(x);
                shareValue = shareValue.add(a).mod(parameterP);
            }
            shares.put(i, shareValue);
        }
        return shares;
    }

    protected BigInteger mergeShares(Map<Integer, BigInteger> shares) {
        BigInteger secret = BigInteger.ZERO;
        for (int i = 1; i <= parameterT; i++) {
            BigInteger numerator = BigInteger.ONE, denominator = BigInteger.ONE;
            for (int j = 1; j <= parameterT; j++) {
                if(i != j){
                    numerator = numerator.multiply(BigInteger.valueOf(-j));
                    denominator = denominator.multiply(BigInteger.valueOf(i - j));
                }
            }
            BigInteger multiplication = shares.get(i).multiply(numerator).multiply(denominator.modInverse(parameterP));
            secret = secret.add(parameterP).add(multiplication).mod(parameterP);
        }
        return secret;
    }
}
