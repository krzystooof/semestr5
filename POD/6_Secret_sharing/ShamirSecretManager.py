from random import randint

from sympy import nextprime


class ShamirSecretManager:
    def __init__(self,number_of_contributions: int = 5, min_number_of_contributions: int = 3, max_value: int = 10000,
                 secret: int = 9999):
        if secret >= max_value:
            raise Exception(f"Secret <{secret}> could not be bigger or equal to max value <{max_value}>")
        self.number_of_contributions =number_of_contributions
        self.min_number_of_contributions = min_number_of_contributions
        self.max_value = max_value
        self.secret = secret
        self.primeP = self.generate_primeP()
        

    def divide_secret(self):
        if self.number_of_contributions < self.min_number_of_contributions:
            raise Exception(
                f"Number of contributions <{self.number_of_contributions}> could not be smaller than min number of "
                f"contributions <{self.min_number_of_contributions}>")
        keys = [(i, self.generate_contribution(self.generate_value(), i, self.primeP),
                 self.primeP)
                for i in range(1, self.number_of_contributions + 1)]
        return keys

    def generate_primeP(self):
        bigger_from_two = max(self.secret, self.number_of_contributions)
        random = randint(bigger_from_two, bigger_from_two * 2)
        return nextprime(random)

    def generate_value(self):
        return randint(0, self.max_value)

    def generate_contribution(self, previous_value, number_of_this_contribution, primeP):
        contribution = self.secret
        for j in range(self.number_of_contributions):
            contribution += (previous_value * pow(number_of_this_contribution, j))
        return contribution % primeP


if __name__ == '__main__':
    print(ShamirSecretManager().divide_secret())
