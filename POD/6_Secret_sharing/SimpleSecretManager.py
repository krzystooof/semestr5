class SimpleSecretManager:
    def __init__(self, number_of_contributions: int = 10, max_value: int = 10000, secret: int = 999):
        if secret >= max_value:
            raise Exception(f"Secret <{secret}> could not be bigger or equal to max value <{max_value}>")
        self.number_of_contributions = number_of_contributions
        self.max_value = max_value;
        self.secret = secret

    def divide_secret(self):
        contributions = []
        for i in range(self.number_of_contributions):
            contribution = self.secret
            for j in range(0, i):
                contribution -= contributions[j]
            contribution %= self.max_value
            contributions.append(contribution)
        return contributions

    def merge_contributions(self, all_contributions):
        secret = 0
        for contribution in all_contributions:
            secret += contribution
        return secret % self.max_value


if __name__ == '__main__':
    secret = 999
    print(f"Secret before dividing: {secret}")
    manager = SimpleSecretManager(secret=secret)
    contributions = manager.divide_secret()
    print(f"Secret after merging: {manager.merge_contributions(contributions)}")
