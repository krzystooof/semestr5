import math
from enum import Enum


class Mode(Enum):
    ENCRYPT = 1
    DECRYPT = 2


def write_matrix(text: str, rows: int, columns: int, mode: Mode):
    text_length = len(text)
    matrix_capacity = rows * columns
    if text_length != matrix_capacity:
        raise IndexError(f"Specified text length <{text_length}> is not equal to matrix capacity <{matrix_capacity}>")
    matrix = [["" for i in range(rows)] for j in range(columns)]
    if mode == Mode.ENCRYPT:
        for char_number in range(len(text)):
            column_number = char_number % columns
            row_number = math.floor(char_number / columns)
            matrix[column_number][row_number] = text[char_number]
    else:
        for char_number in range(len(text)):
            column_number = math.floor(char_number / rows)
            row_number = char_number % rows
            matrix[column_number][row_number] = text[char_number]
    return matrix


def move_columns(matrix, permutation:list, mode: Mode):
    permutation_elements = len(permutation)
    matrix_columns = len(matrix)
    if permutation_elements != matrix_columns:
        raise IndexError(
            f"Specified permutation elements number <{permutation_elements}> is not equal to columns number in matrix <{matrix_columns}>")
    new_matrix = [0 for i in range(len(matrix))]
    number = 0
    for permutation_element in permutation:
        if mode == Mode.ENCRYPT:
            new_matrix[number] = matrix[permutation_element - 1]
        else:
            new_matrix[permutation_element - 1] = matrix[number]
        number += 1
    return new_matrix


def matrix_to_string(matrix, mode: Mode):
    string = ""
    if mode == Mode.ENCRYPT:
        for element in matrix:
                string += ''.join(element)
    else:
        for row in range(len(matrix[0])):
            for column in range(len(matrix)):
                string+= matrix[column][row]
    return string


def crypt(text: str, rows: int, columns: int, permutation:list, mode: Mode):
    if mode == Mode.ENCRYPT:
        text = ''.join(text.split())
    matrix = write_matrix(text, rows, columns, mode)
    matrix = move_columns(matrix, permutation, mode)
    return matrix_to_string(matrix,mode)
