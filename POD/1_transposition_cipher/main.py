import sys

from crypt import crypt, Mode


def get_argument(arg_prefix: str, source=sys.argv):
    for index, arg in enumerate(source):
        if arg_prefix == arg:
            if index + 1 < len(source):
                arg_value = source[index + 1]
            else:
                raise ValueError(f"No argument for {arg_prefix} has been specified")
            if arg_value[0] == "-":
                raise ValueError(f"No argument for {arg_prefix} has been specified")
            return arg_value
    raise IndexError(f"{arg_prefix} has not been specified")


def print_message_and_exit(message: str):
    print(message)
    quit(2)


if __name__ == '__main__':
    source = sys.argv
    arguments = {}
    try:
        arguments['mode'] = get_argument("-m", source=source)
    except IndexError or ValueError:
        print_message_and_exit("PLEASE SPECIFY CRYPT MODE (ENCRYPT or DECRYPT)")

    try:
        arguments['text'] = get_argument("-t", source=source)
    except ValueError:
        print_message_and_exit("PLEASE SPECIFY INPUT TEXT")
    except IndexError:
        pass

    try:
        arguments['file'] = get_argument("-f", source=source)
    except ValueError:
        print_message_and_exit("PLEASE SPECIFY INPUT FILE")
    except IndexError:
        pass

    try:
        arguments['rows'] = get_argument("-r", source=source)
    except IndexError or ValueError:
        print_message_and_exit("PLEASE SPECIFY NUMBER OF ROWS IN TABLE")

    try:
        arguments['columns'] = get_argument("-c", source=source)
    except IndexError or ValueError:
        print_message_and_exit("PLEASE SPECIFY NUMBER OF COLUMNS IN TABLE")

    try:
        arguments['permutaion'] = get_argument("-p", source=source)
    except IndexError or ValueError:
        print_message_and_exit("PLEASE SPECIFY PERMUTATIONS")

    user_input = ""
    try:
        user_input = arguments["text"]
    except KeyError:
        try:
            with open(arguments["file"], "r") as file:
                user_input = file.read()
        except KeyError:
            print_message_and_exit("PLEASE SPECIFY FILE OR TEXT INPUT")

    mode = Mode.ENCRYPT
    if "decrypt" in arguments['mode'].lower():
        mode = Mode.DECRYPT
    permutation = arguments['permutaion'].split(",")
    for i in range(len(permutation)):
        permutation[i] = int(permutation[i])
    print(crypt(text=user_input, rows=int(arguments['rows']), columns=int(arguments['columns']),
          permutation=permutation, mode=mode))
