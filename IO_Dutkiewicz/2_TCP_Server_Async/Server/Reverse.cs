﻿using System;

namespace ReverseNS
{
    class Reverse
    {
        public static string work(String source)
        {
            char[] chars = source.ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }
    }
}
