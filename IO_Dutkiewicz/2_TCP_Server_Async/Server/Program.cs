﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

using System.Threading;
using ReverseNS;

namespace Server
{
    class Program
    {

        static void Main(string[] args)
        {
            Program main = new Program();
            main.start();

            Console.WriteLine("Server accepting connections on port 9999");
            Console.WriteLine("Press any key to stop working...");
            Console.ReadLine();
        }

        TcpListener server = new TcpListener(IPAddress.Any, 9999);

        private void start()
        {
            server.Start();
            accept_connection(); 
        }

        private void accept_connection()
        {
            server.BeginAcceptTcpClient(handle_connection, server);  
        }

        private void handle_connection(IAsyncResult result)  
        {
            accept_connection(); 
            TcpClient client = server.EndAcceptTcpClient(result); 

            byte[] buffer = new byte[50];
            client.GetStream().Read(buffer, 0, buffer.Length);
            string received = Encoding.Default.GetString(buffer);
            Console.WriteLine("Received: " + received);
            string reversed = Reverse.work(received);
            buffer = Encoding.ASCII.GetBytes(reversed);
            client.GetStream().Write(buffer, 0, buffer.Length);
            Console.WriteLine("Responded: " + reversed);

        }

    }
}