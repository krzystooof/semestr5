# Wymagania funkcjonalne
1. Administrator uruchamia aplikację serwera.
2. Serwer rozpoczyna działanie od razu po uruchomieniu aplikacji.
3. Administrator kończy działanie aplikacji serwera.
4. Klient łączy się z aplikacją serwera.
5. Klient przesyła wiadomość do serwera, serwer odpowiada klientowi taką samą wiadomością, lecz w odwrotnej kolejnosci liter.
6. Klient kończy połączenie z aplikacją serwera.
7. Możliwa jest obsługa wielu klientów na raz

# Wymagania pozafunkcjonalne:
1. Aplikacja serwera jest dostarczona w postaci aplikacji konsolowej przeznaczonej na system Windows.
2. W komunikacji klient-serwerwykorzystywany jest protokół komunikacyjny Raw –wiadomości przesyłane są bezpośrednio.
3. W ramach serwera nie jest implementowana obsługa rozłączającego się klienta.
4. W ramach serwera nie jest implementowana informacja o wyłączeniu serwera przysłana do klienta.
5. Serwer utrzymuje kilka połączeń z klientem na raz.
6. Serwer kończy swoje działanie po zamkniecu przez administratora.
7. Implementacja serwera nie jest wersjonowana.

![UML](UML.png)