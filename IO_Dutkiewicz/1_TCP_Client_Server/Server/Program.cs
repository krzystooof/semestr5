﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    class Program
    {
     
        static void Main(string[] args)
        {
            TcpListener server = new TcpListener(IPAddress.Any, 2048);
            server.Start();
            TcpClient client = server.AcceptTcpClient();
            while (true)
            {
                try
                {
                    byte[] buffer = new byte[50];
                    client.GetStream().Read(buffer, 0, buffer.Length);
                    string received = Encoding.Default.GetString(buffer);
                    Console.WriteLine("Received: " + received);
                    string reversed = Reverse.work(received);
                    buffer = Encoding.ASCII.GetBytes(reversed);
                    client.GetStream().Write(buffer, 0, buffer.Length);
                    Console.WriteLine("Responded: " + reversed);
                }
                catch (IOException e) {
                    Console.WriteLine("Client closed connection. Waiting for new connection...\n");
                    client = server.AcceptTcpClient();
                }
            }
        }
    }
}
