﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient client = connectToServer(IPAddress.Parse("127.0.0.1"), 2048);
            NetworkStream stream = client.GetStream();
            Console.WriteLine("Type 'exit' to exit");
            while (true)
            {
                Console.Write("Message: ");
                string input = Console.ReadLine();
                if (input == "exit")
                {
                    client.Close();
                    Console.WriteLine("Closed connection. Press any key to exit...");
                    Console.ReadKey();
                    System.Environment.Exit(0);
                }
                byte[] message = new ASCIIEncoding().GetBytes(input);
                stream.Write(message, 0, message.Length);
                message = new byte[50];
                stream.Read(message, 0, 50);
                string received = Encoding.Default.GetString(message);
                Console.Write("\nReceived: " + received+"\n");
            }
               
        }
        static TcpClient connectToServer(IPAddress address, int port)
        {
            TcpClient client = new TcpClient();
            try
            {
                client.Connect(new IPEndPoint(address, port));
                return client;
            }
            catch (SocketException e)
            {
                Console.WriteLine("Could not connect to server. Retrying in 3 seconds...");
                System.Threading.Thread.Sleep(3000);
                return connectToServer(address,port);
            }
        }
    }
}
