from peewee import *
from openpyxl import load_workbook
from datetime import date

database = SqliteDatabase('people.db')


class Person(Model):
    id = AutoField()
    first_name = CharField()
    last_name = CharField()
    birthday = DateField()

    class Meta:
        database = database


class Pet(Model):
    id = AutoField()
    owner = ForeignKeyField(Person, backref='pets')
    name = CharField()
    animal_type = CharField()

    class Meta:
        database = database


def create_people(people):
    for person in people:
        year = int(person['birth_year'])
        month = int(person['birth_month'])
        day = int(person['birth_day'])
        Person.create(first_name=person['first_name'], last_name=person['last_name'], birthday=date(year, month, day))


def create_pets(pets):
    for pet in pets:
        owner = Person.get(Person.id == pet['owner_id'])
        Pet.create(owner=owner, name=pet['name'], animal_type=pet['animal_type'])


def read_people():
    wb = load_workbook('people.xlsx') #TODO
    sheet_obj = wb.active
    people = []

    for i in range(2, sheet_obj.max_row + 1):
        person = {
            'first_name': sheet_obj.cell(row=i, column=1).value,
            'last_name': sheet_obj.cell(row=i, column=2).value,
            'birth_year': sheet_obj.cell(row=i, column=3).value,
            'birth_month': sheet_obj.cell(row=i, column=4).value,
            'birth_day': sheet_obj.cell(row=i, column=5).value
        }
        people.append(person)

    return people


def read_pets():
    wb = load_workbook('pets.xlsx') #TODO
    sheet_obj = wb.active
    pets = []

    for i in range(2, sheet_obj.max_row + 1):
        pet = {
            'name': sheet_obj.cell(row=i, column=1).value,
            'animal_type': sheet_obj.cell(row=i, column=2).value,
            'owner_id': sheet_obj.cell(row=i, column=3).value
        }
        pets.append(pet)

    return pets


def print_all_people():
    for person in Person.select():
        print(f'{person.id}\t{person.first_name}\t{person.last_name}')
        print(" Pets:")
        for pet in person.pets:
            print(f' * {pet.id}\t{pet.name}\t{pet.animal_type}')
        print()


def get_pets_by_owner_id(owner_id):
    return Person.get(Person.id == owner_id).pets


def get_pets_by_type(animal_type):
    return Pet.select().where(Pet.animal_type == animal_type)


def create_tables():
    with database:
        database.create_tables([Person, Pet])


print("Connecting to database...")
database.connect()
print("Reading entries from xlsx...")
people = read_people()
pets = read_pets()

print("Populating db with data from xlsx...")
create_people(people)
create_pets(pets)

print("Reading entries from db...")
print_all_people()


owner_id = 1
print(f'Reading pets of owner with the id {owner_id}...')
owner_pets = get_pets_by_owner_id(owner_id)
for pet in owner_pets:
    print(f'{pet.id}\t{pet.name}\t{pet.animal_type}')
print()

animal_type = 'dog'
print(f'Printing pets of type {animal_type}...')
dogs = get_pets_by_type(animal_type)
for dog in dogs:
    print(f'{pet.id}\t{pet.name}\t{pet.animal_type}')

database.close()
