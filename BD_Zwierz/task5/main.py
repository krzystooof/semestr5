import bs4 as bs4
import requests
from Crypto.PublicKey import RSA
import sqlite3

print("Generating keys...")
key = RSA.generate(1024)
private_key = key.export_key('PEM')
print(private_key)
public_key = key.publickey().export_key('PEM')
print(public_key)
print(f" Private: {private_key}\n Public: {public_key}")
conn = sqlite3.connect("database.db")
c = conn.cursor()

print("Creating database...")
c.execute('''CREATE TABLE IF NOT EXISTS promotorzy
(id integer primary key autoincrement,
 imie text,
  nazwisko text,
   klucz_publiczny text,
    klucz_prywatny text) ''')

c.execute("INSERT INTO promotorzy(imie, nazwisko, klucz_publiczny, klucz_prywatny) VALUES (?,?,?,?)", ('Jan', 'Nowak', public_key, private_key))
c.execute("INSERT INTO promotorzy(imie, nazwisko, klucz_publiczny, klucz_prywatny) VALUES (?,?,?,?)", ('Jakub', 'Serwetka', public_key, private_key))
c.execute("INSERT INTO promotorzy(imie, nazwisko, klucz_publiczny, klucz_prywatny) VALUES (?,?,?,?)", ('Jerzy', 'Jakubowski', public_key, private_key))
conn.commit()

print("Getting entries from db...")
for row in c.execute('SELECT * FROM promotorzy ORDER BY id'):
    print(row)

conn.close()


wiki_title = "Robert_Lewandowski"
print(f"Getting wiki info for {wiki_title}...")
res = requests.get(f'https://pl.wikipedia.org/wiki/{wiki_title}')
res.raise_for_status()
wiki = bs4.BeautifulSoup(res.text, "html.parser")
print("Saving to wiki.txt...")
with open("wiki.txt", "w", encoding="UTF-8") as f:
    for i in wiki.select('p'):
        f.write(i.getText())


