import sqlite3

import lxml.etree as ET
import qrcode
import qrcode.image.svg

TRUNCATE = True  # False to see entire svg files


def make_svg_qr(data) -> str:
    factory = qrcode.image.svg.SvgImage
    img = qrcode.make(data, image_factory=factory)
    svg: bytes = ET.tostring(ET.ElementTree(img._img))
    return svg.decode("utf-8")


if __name__ == "__main__":
    print("Creating and populating database...")
    conn = sqlite3.connect(":memory:")
    conn.execute("CREATE TABLE sample(id PRIMARY KEY, qr TEXT);")
    conn.execute("INSERT INTO sample(id) VALUES (1),(2),(3);")

    print("Adding qrcode for each entry in db...")
    cursor = conn.execute("SELECT * FROM sample")
    to_update = []
    for row in cursor:
        if row[1] is None:
            modified = (make_svg_qr(str(row[0])), row[0])
            to_update.append(modified)
    for row in to_update:
        conn.execute("UPDATE sample SET qr=? WHERE id=?", row)

    print("Printing each entry...")
    cursor = conn.execute("SELECT * FROM sample")
    for row in cursor:
        data = str(row)
        if TRUNCATE:
            data = (data[:75] + '... ') if len(data) > 75 else data
        print(data)
    conn.close()
