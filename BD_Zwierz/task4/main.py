#!/usr/bin/env python3

import os

from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

dokuwiki_pages_dir = "dokuwiki/config/dokuwiki/data/pages"

engine = create_engine('sqlite:///database.db')
Base = declarative_base()


class Article(Base):
    __tablename__ = "Articles"

    id = Column(Integer, primary_key=True)
    Author = Column(String)
    Subject = Column(String)
    Body = Column(String)
    University = Column(String)

    def __init__(self, author, subject, body, university):
        """"""
        self.Author = author
        self.Subject = subject
        self.Body = body
        self.University = university


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()

print("Populating database...")
with open("populate_database.sql", 'r') as file:
    session.execute(file.readline())


print("Clearing dokuwiki welcome file...")
with open(f"{dokuwiki_pages_dir}/wiki/welcome.txt", "w"):
    pass
try:
    os.mkdir(f"{dokuwiki_pages_dir}/articles")
except FileExistsError:
    pass

for article in session.query(Article).order_by(Article.id):
    print(f"Creating article for item ID - {article.id}...")
    with open(f"{dokuwiki_pages_dir}/articles/{article.id}.txt", "w") as article_page:
        article_page.writelines([f"{article.Author}, {article.University}", article.Subject, article.Body])
    print("Adding article to index file...")
    with open(f"{dokuwiki_pages_dir}/wiki/welcome.txt", "a") as index_file:
        index_file.writelines(
            [f"[[articles:{article.id}| {article.Author}, {article.University} - {article.Subject}]]{os.linesep}"])
print("Done. Now you can open localhost:8080 in your browser and see the results!")
