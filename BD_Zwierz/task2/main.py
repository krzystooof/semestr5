import json as j
import sqlite3
from uuid import uuid4

from names import get_first_name
from redis import Redis

print("Connecting to redis..")
redis = Redis()
cursor = sqlite3.connect(":memory:").cursor()

print("Creating table 'animals'...")
cursor.execute("CREATE TABLE animals (id varchar, name varchar)")

print("Adding 100 random animals to redis...")
animals = (j.dumps({"id": str(uuid4()), "name": get_first_name()}) for _ in range(100))
redis.lpush("animals", *animals)

print("Reading animals from redis...")
redis_animals = [tuple(j.loads(s).values()) for s in redis.lrange("animals", 0, -1)]

print("Adding animals to sqlite db...")
cursor.executemany("INSERT INTO animals VALUES (?, ?)", redis_animals)

print("Reading animals from sqlite...")
sqlite_animals = cursor.execute("SELECT * FROM animals").fetchall()


print("Checking if animals in redis and sqlite are equal...")
try:
    assert sqlite_animals == redis_animals
    print("Done. They are equal")
except AssertionError as msg:
    print(msg)
