import sqlite3
import ET

print("Connecting with database...")
connection = sqlite3.connect(':memory').cursor()

print("Creating table if not exists...")
connection.execute("CREATE TABLE IF NOT EXISTS URZEDY (id INTEGER PRIMARY KEY, nazwa_urzedu STRING)")

tree = ET.parse('KodyUrzedowSkarbowych_v6-0E.xsd') #TODO
root = tree.getroot()

print("Reading xml and populating table...")
for child in root.findall('.//{http://www.w3.org/2001/XMLSchema}enumeration'):
    print(child.tag)
    print(child.attrib['value'])
    print(child.find('.//{http://www.w3.org/2001/XMLSchema}documentation').text)
    krotka = (child.attrib['value'], child.find('.//{http://www.w3.org/2001/XMLSchema}documentation').text)
    connection.execute("INSERT INTO URZEDY(id, nazwa_urzedu) VALUES (?,?)", krotka)

print("Printing each entry...")
cursor = connection.execute("SELECT * FROM URZEDY")
for row in cursor:
    print(row)
connection.close()
